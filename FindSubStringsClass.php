<?php 

class FindSubStrings {
    /**
     * Properties for hold the strings.
     * 
     * @var string $firstString
     * @var string $secondString
     */
    protected $firstString  = '';
    protected $secondString = '';


    /**
     * Assign the compare strings.
     * 
     * @param string $first
     * @param string $second
     * 
     * @return array
     */
    public function __construct (string $firstStr, string $secondStr) {
        $this->firstString  = str_replace ('"', "'", (str_replace ('/', '-', strtolower ($firstStr))));
        $this->secondString = str_replace ('"', "'", (str_replace ('/', '-', strtolower ($secondStr))));
    }

    /**
     * Service method for explode the string to different length parts.
     * 
     * @param string $string
     * 
     * @return array
     */
    protected function getStringsParts (string $string): array {
        mb_internal_encoding ("UTF-8");

        $stringParts = [];
        for ($i = 2; $i <= iconv_strlen ($string); $i++) {
            for ($j = 0; $j < iconv_strlen ($string); $j++) {
                $stringParts[] = mb_substr ($string, $j, $i);
            }
        }

        return $stringParts;
    }

    /**
     * Method, which should return an array with all match which was found in the strings.
     * 
     * @param void
     * 
     * @return array
     */
    public function getMatches (): array {
        
        $firstPartsArray    =   $this->getStringsParts ($this->firstString);
        $secondPartsArray   =   $this->getStringsParts ($this->secondString);

        $matches            =   array_unique (array_intersect ($firstPartsArray, $secondPartsArray));

        foreach ($matches as $index1 => $item1) {
            foreach ($matches as $index2 => $item2) {
                if (iconv_strlen (trim ($item2)) > 1) {
                    if (preg_match ("/($item2)/", $item1) and iconv_strlen ($item2) < iconv_strlen ($item1)) {
                        unset ($matches[$index2]);
                    }
                } else 
                    unset ($matches[$index2]);
            }
        }

        return $matches;
    }
}