<?php

/**
 * Require the class file.
 */
require ('findSubStringsClass.php');

/**
 * Choose and sat any strings.
 * 
 * @var string $string1
 * @var string $string2
 */
$string1 = "Football is my favorite game";
$string2 = "footcort is my best place for watch a ball games.";

/**
 * Create instance of FindSubStringsClass.
 * 
 * @var \FindSubStrings $findSubStrings
 * @var array $matches
 */
$findSubStrings =   new FindSubStrings ($string1, $string2);
$matches        =   $findSubStrings->getMatches ();


/**
 * Echo the result.
 */
echo '<pre>';
print_r ($matches);